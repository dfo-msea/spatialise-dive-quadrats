# Spatialise Dive Survey Quadrats

__Main author:__  Jessica Nephin  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: jessica.nephin@gmail.com | tel: 250-363-6564


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
  + [Steps](#steps)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)



## Objective
To georeference dive survey quadrat observations by generating points at specified intervals along extended transect lines.


## Summary
DFO nearshore habitat mapping and shellfish dive surveys record only the start
and end position of the transect. Species and habitat observations are recorded
at each quadrat along the transect. There is a need to approximate the position
of quadrats along the transect for nearshore species distribution modelling.


## Status
Completed


## Contents
The spatialising tool is run from the SpatialiseDiveQuadrats.R script. The script returns the spatialised points, a log file and some additional files with information on any removed points or transects and the extended transect lines.


## Methods
Line segments are created from extended start and end transect points. Start and
end points were extended along the transect bearing to account for imprecision
in the position of the start and end position. The shallow point of the transect
(the end closest to the coastline) are only extended the distance from the end
point to the highwater coastline. In order to approximate the locations of the
quadrat observations, the extended transect line is broken up into points at a
certain distance along the line. Depth is extracted from a bathymetry raster
at each point along transect. Quadrats are then assigned to a point by finding
the closest match between the extracted depth and the observed quadrat depth.
Because quadrats are typically at a finer resolution than the 20 by 20m
bathymetry raster, several quadrats may be assigned to one point (3 quadrats per
point on average for BHM dive surveys from 2013 to 2015).

### Steps
1.  Create spatial lines from extended start and end points
      * Extend from deep end by 'dist_extend' distance
      * Extend from shallow end by distance to shoreline or 'threshold'
2.  Generate points along extended transect lines at a selected distance
3.  Extract depth from bathymetry raster at points along lines
4.  Assign quadrats to points using closest depth match
5.  Merge spatialized points with quadrat observations
6.  Groups species data (presence/absence) by spatial points


## Requirements
1.  A bathymetry raster and a high water line coastline
2.  Dive quadrat data with fields: Survey, Year, Month, Day, HKey, Quadrat, CorDepthM
    *   'HKey' field is a unique identifier for every transect
    *   'Quadrat' field is a unique identifier for each quadrat within a transect
    *   'CorDepthM' field is the chart datum corrected quadrat depth in meters
    *   Lat and long must be decimal degrees represented with fields: LonShallow, LatShallow, LonDeep, LatDeep
3.  When presence/absence are represented by 0/1 values, the field containing 0/1 values must be named 'SpNum'
4.  Required R packages are rgdal, raster, rgeos, geoshere, Hmisc and reshape2.


## Caveats
Not all transects can be spatialized into point locations along the transect using this method. Ideally, transects require a start and end position. In cases where there is only a start or an end point, or the start and end points are the same, the single x,y point is retained and quadrats are aggregated to that point when they are within the chosen depth difference cut-off. All other quadrats will be removed.

Currently only works with presence/absence data. Could be used for abundance with a few modifications.


## Uncertainty
The difference between the quadrat depth and bathymetry depth can be used as a proxy for the uncertainty in the position of the points. When the difference in depth was greater than the selected depth distance cut-off (e.g. 10 m) the transect is removed because the position of either the start and end point of the transect is thought likely to be inaccurate. For BHM dive surveys from 2013 to 2015 the difference in depth was 2 m on average.


## Acknowledgements
Sarah Davies, Cole Fields, Candice St. Germain, Ed Gregr and Michael Peterman
